## Komponens alapú szoftverfejlesztés 2015 ##

Heurisztika komponensek a *Potyogós Amőba* és az *Agarak és rókák* játékokhoz.

* [Forrás](https://bitbucket.org/lordsab/komponensalapu/src)

* [Letöltés](https://bitbucket.org/lordsab/komponensalapu/downloads)


Balogh Szabolcs

2015