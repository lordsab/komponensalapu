﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;

namespace Komponensalapu.PotyogosHeur
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IPAddress ip = IPAddress.Loopback;
                UInt16 port = 0;

                if (args.Length == 2)
                {
                    ip = IPAddress.Parse(args[0]);
                    port = UInt16.Parse(args[1]);
                }
                else if (args.Length == 1)
                {
                    port = UInt16.Parse(args[0]);
                }
                else
                {
                    Console.WriteLine("Hasznalat:");
                    Console.WriteLine("PotyogosHeur.exe <port>");
                    Console.WriteLine("PotyogosHeur.exe <ip> <port>");
                    return;
                }

                PotyogosHeur ph = PotyogosHeur.Instance;

                ph.Init(ip, port);
            }
            catch(Exception e)
            {
                Console.WriteLine("HEURISTICS: Error: {0}", e.Message);
            }
        }
    }
}
