﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Komponensalapu.PotyogosHeur
{
    class PotyogosHeur
    {
        static PotyogosHeur instance;
        static public PotyogosHeur Instance
        {
            get
            {
                if (instance == null)
                    instance = new PotyogosHeur(); 
                return instance;
            }
        }
        private PotyogosHeur() : base() { }

        HeurComms.CommunicationMgr comms;
        public void Init(IPAddress gameTreeIP, UInt16 gameTreePort)
        {
            comms = new HeurComms.CommunicationMgr(gameTreeIP, gameTreePort);

            comms.RegisterErrorCallback(ErrorHandler);
            comms.RegisterCallback("EVAL", EvalHandler);
            comms.RegisterCallback("CLOSE", CloseHandler);

            comms.Connect();
        }

        private void CloseHandler(string messageType, JObject content)
        {
            comms.Close();
        }

        public void ErrorHandler(Exception innerException, String messageContents)
        {
            Console.WriteLine("HEURISTICS: Error: {0}, Message: {1}", innerException.Message, messageContents == null ? "N/A" : messageContents);
        }

        public void EvalHandler(String messageType, JObject content)
        {
            PotyogosState state = PotyogosState.ParseJSON(content["state"] as JObject);

            if (state.IsValid)
            {
                Int32 goodness = EvaluateState(state);

                Console.WriteLine("HEURISTICS: Goodness: {0}", goodness);

                comms.Send("EVALRE", JObject.FromObject(new
                {
                    stateValue = goodness
                }));
            }
            else
            {
                Console.WriteLine("HEURISTICS: Invalid state!");
            }
        }

        
        private readonly Int32[] scoreBases = new Int32[] { 1, 2, 50, 500, 8000 }; // hány pontot ér egy 4-esben szereplő azonos szín
        public Int32 EvaluateState(PotyogosState state)
        {
            Tuple<Int32, Int32> horizontalScores = EvalDirection(state, state.rows, state.cols - 3, (i, k) => i, (j, k) => (Byte)(j + k));
            Tuple<Int32, Int32> verticalScores = EvalDirection(state, state.rows - 3, state.cols, (i, k) => (Byte)(i + k), (j, k) => j);
            Tuple<Int32, Int32> diagonal1Scores = EvalDirection(state, state.rows - 3, state.cols - 3, (i, k) => (Byte)(i + k), (j, k) => (Byte)(j + k));
            Tuple<Int32, Int32> diagonal2Scores = EvalDirection(state, state.rows - 3, state.cols - 3, (i, k) => (Byte)(state.rows - 1 - i - k), (j, k) => (Byte)(j + k));

            Int32 p1score = horizontalScores.Item1 + verticalScores.Item1 + diagonal1Scores.Item1 + diagonal2Scores.Item1;
            Int32 p2score = horizontalScores.Item2 + verticalScores.Item2 + diagonal1Scores.Item2 + diagonal2Scores.Item2;

            return p1score - p2score;
        }

        private delegate Byte IndexTransformation(Byte index, Byte k);

        private Tuple<Int32, Int32> EvalDirection(PotyogosState state, Int32 maxI, Int32 maxJ, IndexTransformation transI, IndexTransformation transJ)
        {
            Int32 p1score = 0;
            Int32 p2score = 0;

            // vegyük a vízszíntes négyeseket
            for (Byte i = 0; i < maxI; ++i)
            {
                for (Byte j = 0; j < maxJ; ++j)
                {
                    // számoljuk meg, hány X/O van a négyesben
                    Byte p1count = 0;
                    Byte p2count = 0;
                    for (Byte k = 0; k < 4; ++k)
                    {
                        if (state[transI(i, k), transJ(j, k)] == 1) ++p1count;
                        if (state[transI(i, k), transJ(j, k)] == 2) ++p2count;
                    }

                    // ha valamelyik játékosnak nincs egyetlen X/O sem, akkor ez egy értékes négyes, növeljük a score-t
                    p1score += p2count == 0 ? scoreBases[p1count] : 0;
                    p2score += p1count == 0 ? scoreBases[p2count] : 0;
                    
                }
            }
            return new Tuple<int, int>(p1score, p2score);
        }
    }
}
