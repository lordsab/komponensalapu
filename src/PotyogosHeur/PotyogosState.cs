﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Komponensalapu.PotyogosHeur
{
    class PotyogosState
    {
        Byte[][] _board;
        Int32 _rowsCount;
        Int32 _colsCount;

        public Byte this[int i, int j]
        {
            get
            {
                return _board[i][j];
            }
            private set
            {
                _board[i][j] = value;
            }
        }

        public Int32 rows { get { return _rowsCount; } }
        public Int32 cols { get { return _colsCount; } }

        public static PotyogosState ParseJSON(JObject json)
        {
            PotyogosState state = new PotyogosState();

            state._board = (
                from row in json["board"]
                select (
                    from cell in row
                    select Byte.Parse(cell.ToString())
                ).ToArray()
            ).ToArray();

            state._rowsCount = state._board.Count();
            state._colsCount = state._board.FirstOrDefault().Count();

            return state;
        }

        public Boolean IsValid
        {
            get
            {
                Boolean valid = true;

                // Méret
                valid &= _board.Length >= 4 && _board.All(r => r.Length >= 4);
                // mezők: 0,1,2
                valid &= _board.All(r => r.All(c => c >= 0 && c < 3));

                return valid;
            }
        }
    }
}
