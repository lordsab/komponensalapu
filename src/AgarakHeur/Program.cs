﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;

namespace Komponensalapu.AgarakHeur
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IPAddress ip = IPAddress.Loopback;
                UInt16 port = 12345; // csak debug

                if (args.Length == 2)
                {
                    ip = IPAddress.Parse(args[0]);
                    port = UInt16.Parse(args[1]);
                }
                else if (args.Length == 1)
                {
                    port = UInt16.Parse(args[0]);
                }
                else
                {
                    Console.WriteLine("Hasznalat:");
                    Console.WriteLine("AgarakHeur.exe <port>");
                    Console.WriteLine("AgarakHeur.exe <ip> <port>");
                    return;
                }

                AgarakHeur ph = AgarakHeur.Instance;

                ph.Init(ip, port);
            }
            catch (Exception e)
            {
                Console.WriteLine("HEURISTICS: Error: {0}", e.Message);
            }
        }
    }
}
