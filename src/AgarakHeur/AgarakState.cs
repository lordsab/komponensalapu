﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Komponensalapu.AgarakHeur
{

    enum AgarakFields : byte
    {
        FieldFree = 0,
        FieldAgar = 1,
        FieldRoka = 2
    }

    class AgarakState
    {
        AgarakFields[][] _board;

        public AgarakFields this[int i, int j]
        {
            get
            {
                return _board[i][j];
            }
            private set
            {
                _board[i][j] = value;
            }
        }

        public AgarakFields[][] Board
        {
            get
            {
                return _board;
            }
        }

        public static AgarakState ParseJSON(JObject json)
        {
            AgarakState state = new AgarakState();

            state._board = (
                from row in json["board"]
                select (
                    from cell in row
                    select (AgarakFields)byte.Parse(cell.ToString())
                ).ToArray()
            ).ToArray();

            state.Size = state._board.Length;

            return state;
        }

        public Int32 Size { get; private set; }

        public Boolean IsValid
        {
            get
            {
                Boolean valid = true;

                // Méret
                valid &= _board.Length == Size && _board.All(r => r.Length == Size);
                // 2 róka
                valid &= _board.SelectMany(r => r.Where(c => c == AgarakFields.FieldRoka)).Count() == 2;
                // 4 fél sornyi agár
                valid &= _board.SelectMany(r => r.Where(c => c == AgarakFields.FieldAgar)).Count() == Size * 2;

                return valid;
            }
        }

    }
}
