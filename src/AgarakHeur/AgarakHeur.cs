﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Komponensalapu.AgarakHeur
{
    class AgarakHeur
    {
        static AgarakHeur instance;
        static public AgarakHeur Instance
        {
            get
            {
                if (instance == null)
                    instance = new AgarakHeur(); 
                return instance;
            }
        }
        private AgarakHeur() : base() { }

        HeurComms.CommunicationMgr comms;
        public void Init(IPAddress gameTreeIP, UInt16 gameTreePort)
        {
            comms = new HeurComms.CommunicationMgr(gameTreeIP, gameTreePort);

            comms.RegisterErrorCallback(ErrorHandler);
            comms.RegisterCallback("EVAL", EvalHandler);
            comms.RegisterCallback("CLOSE", CloseHandler);

            comms.Connect();
        }

        private void CloseHandler(string messageType, JObject content)
        {
            comms.Close();
        }

        public void ErrorHandler(Exception innerException, String messageContents)
        {
            Console.WriteLine("HEURISTICS: Error: {0}, Message: {1}", innerException.Message, messageContents == null ? "N/A" : messageContents);
        }

        public void EvalHandler(String messageType, JObject content)
        {
            AgarakState state = AgarakState.ParseJSON(content["state"] as JObject);

            if (state.IsValid) { 
                Int32 goodness = EvaluateState(state);//.Result;

                Console.WriteLine("HEURISTICS: Goodness: {0}", goodness);

                comms.Send("EVALRE", JObject.FromObject(new {
                    stateValue = goodness
                }));
            }
            else
            {
                Console.WriteLine("HEURISTICS: Invalid state!");
            }
        }

        public Int32 EvaluateState(AgarakState state)
        {
            Byte rokaFound = 0;

            Int32 score = 0;

            for (int i = 0; i < state.Size; ++i)
            {
                Byte agarCount = 0;
                for (int j = 0; j < state.Size; ++j)
                {
                    switch (state[i, j])
                    {
                        case AgarakFields.FieldAgar:
                            ++agarCount;
                            break;
                        case AgarakFields.FieldRoka:
                            ++rokaFound;
                            break;
                    }
                }
                score += (10 + (state.Size - i) - (2 - rokaFound) * 20) * agarCount;
            }

            return score;
        }
    }
}
