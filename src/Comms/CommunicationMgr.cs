﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace Komponensalapu.HeurComms
{
    public class CommunicationMgr
    {
        private const Int32 maxPacketSize = 4096;
        protected IPAddress tcpTargetIP;
        protected UInt16 tcpTargetPort;

        private static Object syncObject = new Object();

        public CommunicationMgr(IPAddress _tcpTargetIP, UInt16 _tcpTargetPort)
        {
            tcpTargetIP = _tcpTargetIP;
            tcpTargetPort = _tcpTargetPort;
            socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            messageHandlers = new Dictionary<string, ReceivedMessageDelegate>();
        }

        ~CommunicationMgr()
        {
            Close();
        }


        #region Socket

        public void Connect()
        {
            socket.Connect(tcpTargetIP, tcpTargetPort);
            socket.SendBufferSize = maxPacketSize;
            Console.WriteLine("HEURISTICS: Connected to gametree at {0}:{1}", tcpTargetIP, tcpTargetPort);
            Listen();
        }

        public void Close()
        {
            if (tcpListenerThread.ThreadState == ThreadState.Running ||
                tcpListenerThread.ThreadState == ThreadState.WaitSleepJoin ||
                tcpListenerThread.ThreadState == ThreadState.Suspended ||
                tcpListenerThread.ThreadState == ThreadState.Background)
            { 
                tcpListenerThread.Abort();
            }

            if (socket.Connected)
            {
                socket.Close();
            }
        }




        private Boolean listening = false;
        private Object threadStarterMutex = new Object();
        private Thread tcpListenerThread;
        private Socket socket;

        private Socket connectedSocket
        {
            get
            {
                if (!socket.Connected)
                {
                    Connect();
                }
                return socket;
            }
        }

        public bool Send(String messageType, JObject messageJSON)
        {
            messageJSON["messageType"] = messageType;

            var encoder = new ASCIIEncoding();
            byte[] rawMessage = encoder.GetBytes(messageJSON.ToString(Formatting.None) + "\n");

            lock (syncObject)
            {
                connectedSocket.Send(rawMessage);
            }

            //Console.WriteLine("HEURISTICS: Sent {0}", messageType);

            return true;
        }

        private void Listen()
        {
            lock (threadStarterMutex)
            {
                if (!listening)
                {
                    tcpListenerThread = new Thread(new ThreadStart(HandleClient));
                    tcpListenerThread.Start();
                }
                else
                {
                    new InvalidOperationException("Already Listening!");
                }
            }
        }

        private void HandleClient()
        {
            Console.WriteLine("HEURISTICS: Listening to gametree");

            listening = true;

            byte[] rawBytes = new byte[maxPacketSize];
            int bytesRead;

            while (true)
            {
                try {
                    bytesRead = 0;
                  
                    try
                    {
                        lock (syncObject)
                        {
                            bytesRead = connectedSocket.Receive(rawBytes);
                        }
                        
                    }
                    catch
                    {
                      break;
                    }
                  
                    if (bytesRead == 0)
                    {
                      break;
                    }
                  
                    //message has successfully been received
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    String message = encoder.GetString(rawBytes, 0, bytesRead);

                    if (!Regex.IsMatch(message, @"^\s*$"))
                    {
                        try
                        {
                            JObject JSONmessage = JObject.Parse(message);

                            String messageType = JSONmessage["messageType"].ToString();

                            if (messageHandlers.ContainsKey(messageType))
                            {
                                //Console.WriteLine("HEURISTICS: Received {0}", messageType);
                                messageHandlers[messageType](messageType, JSONmessage);
                            }
                            else
                            {
                                Console.WriteLine("HEURISTICS: Dropped message of unknown type {0}", messageType);
                            }
                        }
                        catch (Exception e)
                        {
                            errorHandler(e, message);
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    if (socket.Connected)
                        socket.Close();
                    listening = false;
                    // ThreadAbortException auto újra dobódik
                }
                catch (Exception e)
                {
                    errorHandler(e, null);
                }
            }

            Console.WriteLine("HEURISTICS: Connection terminated.");

            socket.Close();

            listening = false;
        }

        #endregion

        #region Callbacks

        public delegate void ReceivedMessageDelegate(String messageType, JObject content);
        public delegate void ErrorHandlerDelegate(Exception innerException, String messageContents);

        private Dictionary<String, ReceivedMessageDelegate> messageHandlers;
        private ErrorHandlerDelegate errorHandler;

        public bool RegisterCallback(String messageType, ReceivedMessageDelegate callback)
        {
            if (messageHandlers.ContainsKey(messageType))
            {
                return false;
            }

            messageHandlers.Add(messageType, callback);
            return true;
        }

        public void RegisterErrorCallback(ErrorHandlerDelegate callback)
        {
            errorHandler = callback;
        }
        #endregion

    }
}
